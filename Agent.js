/**
 * Agent Class
 * Core class for all agents
 * 
 * @class Agent
 */
class Agent {
    constructor(data) {
        this.id = data.agentId;
        this.name = data.agentName;
        this.type = data.agentType;
        this.balance = data.agentBalance;
        this.portfolio = data.portfolio;
        this.initialize(data.Market);
    }

    initialize(Market) {
        this.behavior = new AgentBehavior(this, Market);
    }

    tick(Market) {
        let random = Math.floor(Math.random() * 100);
        if (random < 10) {
            this.behavior.tick(Market, this);
        }
    }

    placeOrder(Market, security, quantity, price, orderType, orderExecution) {
        if (this.validateOrder(quantity, price)) {
            if (orderType === "buy") {
                this.updateBalance(-1 * (price * quantity));
            }
            Market.broker.placeOrder(this.id, security, quantity, price, orderType, orderExecution);
        }
    }

    cancelOrder(Market, order) {
        Market.broker.cancelOrder(Market, order.id);
    }

    updateBalance(amount) {
        amount = parseFloat(amount).toFixed(2);
        let oldBalance = parseFloat(this.balance).toFixed(2);
        let newBalance = parseFloat(parseFloat(oldBalance) + parseFloat(amount)).toFixed(2);
        this.balance = newBalance;
    }

    validateOrder(quantity, price) {
        if (this.balance < price * quantity) {
            return false;
        }
        return true;
    }
}