/**
 * ClearingHouse
 * Holds Orders and logic for matching them
 */
class ClearingHouse {
    constructor(security) {

        /**
         * Security Ticker Symbol
         */
        this.security = security;

        /**
         * List of Limit Buy Orders
         */
        this.limitBuyOrders = [];

        /**
         * List of Limit Sell Orders
         */
        this.limitSellOrders = [];

        /**
         * List of Market Buy Orders
         */
        this.marketBuyOrders = [];

        /**
         * List of Market Sell Orders
         */
        this.marketSellOrders = [];

        /**
         * List of Completed Orders
         */
        this.closedOrders = [];

        /**
         * OrderProcessor Instance
         */
        this.OrderProcessor = new OrderProcessor();
    }

    /**
     * Adds an order to the appropriate order list
     * @param {Order} order 
     */
    addOrder(order) {
        if (order.ticker === this.security) {
            if (order.type === "buy") {
                if (order.executionType === "limit") {
                    this.limitBuyOrders.push(order);
                } else {
                    this.marketBuyOrders.push(order);
                }
            } else {
                if (order.executionType === "limit") {
                    this.limitSellOrders.push(order);
                } else {
                    this.marketSellOrders.push(order);
                }
            }
        } else {
            console.log("Clearing House - " + this.security + ": Order Security Does Not Match Clearing House Security");
        }
    }

    getLowestLimitSellOrder() {
        let limitSellOrder = this.limitSellOrders.sort((a, b) => a.price - b.price)[0];
        if (limitSellOrder && limitSellOrder.type !== "sell") {
            console.log("Clearing House - " + this.security + ": Order is not a sell order");
        }
        return limitSellOrder;
    }

    getHighestLimitBuyOrder() {
        return this.limitBuyOrders.sort((a, b) => b.price - a.price)[0];
    }

    getLatestMarketBuyOrder() {
        return this.marketBuyOrders.sort((a, b) => b.time - a.time)[0];
    }

    getLatestMarketSellOrder() {
        return this.marketSellOrders.sort((a, b) => a.time - b.time)[0];
    }

    processOpenOrders() {
        /**
         * Process All Open Orders
         * 1. Process Limit Orders
         * 2. Process Market Orders
         */
        let processedLimitOrders = this.processLimitOrders();
        let processedMarketOrders = this.processMarketOrders();
        if (processedLimitOrders || processedMarketOrders) {
            // If any orders were procesed, recursively call this function
            return this.processOpenOrders();
        }
        // If no orders were processed, return false
        return false;
    }

    /**
     * Process Limit Orders
     * 1. Check if there are any limit buy orders
     * 2. Check if there are any limit sell orders
     * 3. If there are both, check if the lowest limit sell order is lower than the highest limit buy order
     * 4. If it is, process the order, return true
     * 5. If it is not, do nothing, return false
     * 
     * @returns {boolean}
     */
    processLimitOrders() {
        let lowestLimitSellOrder = this.getLowestLimitSellOrder();
        let highestLimitBuyOrder = this.getHighestLimitBuyOrder();

        if (lowestLimitSellOrder && highestLimitBuyOrder) {
            if (lowestLimitSellOrder.type !== "sell" || highestLimitBuyOrder.type !== "buy") {
                console.log("ProcessLimitOrders: Order Type Mismatch");
                console.log({
                    lowestLimitSellOrder: lowestLimitSellOrder,
                    highestLimitBuyOrder: highestLimitBuyOrder
                });
                return false;
            }
            if (parseFloat(lowestLimitSellOrder.price) <= parseFloat(highestLimitBuyOrder.price)) {
                return this.executeOrders(lowestLimitSellOrder, highestLimitBuyOrder);
            } else {
                /**
                 * If the lowest limit sell order is not lower than the highest limit buy order
                 */
                return false;
            }
        }
        return false;
    }

    processMarketOrders() {
        /**
         * Process Market Orders
         * 1. Process The Latest Market Buy Orders Agains The Limit Sell Orders
         * 2. Process The Latest Market Sell Orders Agains The Limit Buy Orders
         * 3. Process The Latest Market Buy Orders Agains The Latest Market Sell Orders
         */
        let processedMarketBuy = this.processLatestMarketBuyOrders();
        let processedMarketSell = this.processLatestMarketSellOrders();
        let processedLatestBuy = this.processLatestMarketBuyOrdersAgainstLatestMarketSellOrders();
        if (processedMarketBuy || processedMarketSell || processedLatestBuy) {
            return true;
        }
        return false;
    }

    processLatestMarketBuyOrders() {
        /**
         * Process Latest Market Buy Orders
         * 1. Get the latest market buy order
         * 2. Get the lowest limit sell order
         * 3. Execute the trade
         */
        let latestMarketBuyOrder = this.getLatestMarketBuyOrder();
        let lowestLimitSellOrder = this.getLowestLimitSellOrder();
        if (latestMarketBuyOrder && lowestLimitSellOrder) {
            return this.executeOrders(latestMarketBuyOrder, lowestLimitSellOrder);
        }
        return false;
    }


    /**
     * Process Latest Market Sell Orders
     * 
     * 1. Get the latest market sell order
     * 2. Get the highest limit buy order
     * 3. If both are found, Execute the trade
     * 
     * @returns boolean
     */
    processLatestMarketSellOrders() {
        let latestMarketSellOrder = this.getLatestMarketSellOrder();
        let highestLimitBuyOrder = this.getHighestLimitBuyOrder();
        if (latestMarketSellOrder && highestLimitBuyOrder) {
            return this.executeOrders(latestMarketSellOrder, highestLimitBuyOrder);
        }
        return false;
    }

    /**
     * Process Latest Market Buy Orders Against Latest Market Sell Orders
     * 1. Get the latest market buy order
     * 2. Get the latest market sell order
     * 3. If both are found, Execute the trade
     * 
     * @returns boolean
     */
    processLatestMarketBuyOrdersAgainstLatestMarketSellOrders() {
        let latestMarketBuyOrder = this.getLatestMarketBuyOrder();
        let latestMarketSellOrder = this.getLatestMarketSellOrder();
        if (latestMarketBuyOrder && latestMarketSellOrder) {
            return this.executeOrders(latestMarketBuyOrder, latestMarketSellOrder);
        }
        return false;
    }

    /**
     * Execute Orders
     * 1. Get the order processor
     * 2. Process the orders
     * 3. Remove the orders from the order lists
     * 4. Add the orders to the closed orders list
     * 
     * @param {Order} orderA 
     * @param {Order} orderB 
     * @returns boolean
     */
    executeOrders(orderA, orderB) {
        if (orderA.type === orderB.type) {
            console.log("Clearing House - " + this.security + ": Cannot Execute Orders Of The Same Type");
            console.log(orderA, orderB);
            return false;
        }
        let orderAId = orderA.id;
        let orderBId = orderB.id;
        if (!this.OrderProcessor.hasOrders()) {
            this.OrderProcessor.addOrders(orderA, orderB);
            if (this.OrderProcessor.processOrders()) {
                let newOrderA = this.OrderProcessor.orderA;
                let newOrderB = this.OrderProcessor.orderB;

                /**
                 * If Orders are closed, remove them from the order lists and add them to the closed orders list
                 * If Orders are partial, update them in the order lists
                 */
                if (newOrderA.status === "closed") {
                    this.removeOrder(orderAId, newOrderA);
                    this.closedOrders.push(newOrderA);
                } else if (newOrderA.status === "partial") {
                    this.updateOrderById(orderAId, newOrderA);
                }
                if (newOrderB.status === "closed") {
                    this.closedOrders.push(newOrderB);
                    this.removeOrder(orderBId, newOrderB);
                } else if (newOrderB.status === "partial") {
                    this.updateOrderById(orderBId, newOrderB);
                }
                this.OrderProcessor.clearOrders();
                return true;
            }
            this.OrderProcessor.clearOrders();
            console.log("Clearing House -" + this.security + ": Orders Not Processed");
        } else {
            console.log("Order Processor is busy");
        }        
        return false;
    }

    removeOrder(id, order) {
        let orderList = this.getOrderListByType(order.type, order.executionType);
        let index = orderList.findIndex((order) => order.id === id);
        if (index > -1) {
            orderList.splice(index, 1);
            return true;
        }
        return false;
    }


    updateOrderById(id, order) {
        let orderList = this.getOrderListByType(order.type, order.executionType);
        if (orderList) {
            let orderIndex = orderList.findIndex((order) => {
                return order.id === id;
            });
            if (orderIndex > -1) {
                orderList[orderIndex] = order;
            }
        }
        this.updateOrderList(orderList);
    }

    updateOrderList(orderList) {
        let type = orderList[0].type;
        let executionType = orderList[0].executionType;
        if (type === "buy") {
            if (executionType === "limit") {
                this.limitBuyOrders = orderList;
            } else if (executionType === "market") {
                this.marketBuyOrders = orderList;
            }
        } else if (type === "sell") {
            if (executionType === "limit") {
                this.limitSellOrders = orderList;
            } else if (executionType === "market") {
                this.marketSellOrders = orderList;
            }
        }
    }
    getOrderListByType(type, executionType) {
        if (type === "buy") {
            if (executionType === "limit") {
                return this.limitBuyOrders;
            } else if (executionType === "market") {
                return this.marketBuyOrders;
            }
        } else if (type === "sell") {
            if (executionType === "limit") {
                return this.limitSellOrders;
            } else if (executionType === "market") {
                return this.marketSellOrders;
            }
        }
    }

    getClosedOrders() {
        return this.closedOrders;
    }

    hasClosedOrders() {
        return this.closedOrders.length > 0;
    }

    clearClosedOrders() {
        this.closedOrders = [];
    }

    getOrdersByAgentId(agentId) {
        let orders = [];
        orders = orders.concat(this.limitBuyOrders.filter((order) => order.agentId === agentId));
        orders = orders.concat(this.limitSellOrders.filter((order) => order.agentId === agentId));
        orders = orders.concat(this.marketBuyOrders.filter((order) => order.agentId === agentId));
        orders = orders.concat(this.marketSellOrders.filter((order) => order.agentId === agentId));
        return orders;
    }

    getOrderById(id) {
        let orders = [];
        orders = orders.concat(this.limitBuyOrders.filter((order) => order.id === id));
        orders = orders.concat(this.limitSellOrders.filter((order) => order.id === id));
        orders = orders.concat(this.marketBuyOrders.filter((order) => order.id === id));
        orders = orders.concat(this.marketSellOrders.filter((order) => order.id === id));
        return orders[0];
    }
}