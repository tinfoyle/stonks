class Config {
    constructor() {
        this.tickRate = 1000;
        this.maxGraphTicks = 200;
        this.numAgents = 1000;
        this.buyColor = "rgba(54, 162, 235, 0.2)";
        this.buyBorderColor = "rgba(54, 162, 235, 1)";
        this.sellColor = "rgba(255, 99, 132, 0.2)";
        this.sellBorderColor = "rgba(255, 99, 132, 1)";
        this.lastTradedColor = "rgba(255, 206, 86, 0.2)";
        this.lastTradedBorderColor = "rgba(255, 206, 86, 1)";
    }

    setMetaData() {
        this._version = "0.0.1";
        this._name = "Engine";
        this._author = "Tinfoyle";
        this._description = "JS/HTML5 Stock Market Game Engine";
        this._website = "https://stocksim2022.com";
        this._license = "Not yours to take";
    }
}