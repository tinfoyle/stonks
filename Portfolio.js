class Portfolio {
  constructor() {
    this.holdings = {};
    this.orderHistory = [];
  }

  /**
   * Adds a Closed Order to the Portfolio
   * @param {Order} order 
   */
  addCompletedOrder(order) {
    // If the Holding does not exist, create it
    if (!this.holdings[order.ticker]) {
      this.createNewHolding(order);
    }

    // Add the Order to the Order History & Update the Holding
    this.updateHolding(order);
    this.orderHistory.push(order);
  }

  updateHolding(order) {
    let holding = this.holdings[order.ticker];
    if (order.type === "buy") {
      // Get new total quantity
      let totalQuantity = holding.quantity + order.quantity;

      // Get new total cost
      let totalCost = holding.averagePrice * holding.quantity + order.price * order.quantity;

      // Get new average price
      holding.averagePrice = totalCost / totalQuantity;

      // Update quantity
      holding.quantity = totalQuantity;
    } else if (order.type === "sell") {
      //Get new total quantity
      let totalQuantity = holding.quantity - order.quantity;

      // Update quantity
      holding.quantity = totalQuantity;

      // If the holding is now empty, remove it
      if (holding.quantity === 0) {
        delete this.holdings[order.ticker];
      }
    }
  }

  getValueOfHoldings(ticker = null) {
    if (ticker) {
      let holding = this.holdings[ticker];
      return holding.quantity * holding.averagePrice;
    } else {
      let totalValue = 0;
      for (let holding in this.holdings) {
        totalValue += this.holdings[holding].quantity * this.holdings[holding].averagePrice;
      }
      return totalValue;
    }
  }

  getHoldingsQuantity(ticker = null) {
    if (ticker) {
      return this.holdings[ticker].quantity;
    } else {
      let totalQuantity = {};
      for (let holding in this.holdings) {
        totalQuantity[holding] = this.holdings[holding].quantity;
      }
      return totalQuantity;
    }
  }
  createNewHolding(order) {
    let holding = {
      ticker: order.ticker,
      quantity: 0,
      averagePrice: 0
    };
    this.holdings[holding.ticker] = holding;
  }

  getSharesByTicker(ticker) {
    if (this.holdings[ticker]) {
      return this.holdings[ticker].quantity;
    } else {
      return 0;
    }
  }
}