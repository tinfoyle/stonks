class Security {
    constructor(name, ticker, lastTraded) {
        /**
         * Security Long Form Name
         * @type {string}
         */
        this.name = name;

        /**
         * Security Ticker Symbol
         * @type {string}
         */
        this.ticker = ticker;

        /**
         * Last Traded Price
         */
        this.lastTraded = lastTraded;

        /**
         * Last Traded Price History
         */
        this.priceHistory = [];

        /**
         * Percent Change in Price
         * 
         * @type {number}
         */
        this.delta = 0;

        /**
         * Volume of Units Traded
         * @type {number}
         */
        this.volume = 0;

        /**
         * Clearing House Instance
         * @type {ClearingHouse}
         */
        this.clearingHouse = new ClearingHouse(this.ticker);

        /**
         * Dividend Yield
         * @type {number}
         */
        this.dividend = 0.05;

        this.dividendInterval = 10;

        this.dividendTime = Math.floor(Date.now() / 1000 + this.dividendInterval);

    }

    getAskPrice() {
        return this.clearingHouse.getLowestLimitSellOrder()?.price || this.lastTraded;
    }

    getBidPrice() {
        return this.clearingHouse.getHighestLimitBuyOrder()?.price || this.lastTraded;
    }

    tick() {
        this.clearingHouse.processOpenOrders();
        this.updateVolume();
    }

    getLastTraded() {
        return this.lastTraded;
    }

    updateVolume() {
        let closedOrders = this.clearingHouse.getClosedOrders();
        let volume = 0;
        closedOrders.forEach(order => {
            volume += order.quantity;
        });
        this.volume = volume;
    }

    updateLastTraded(lastTradedPrice) {
        this.priceHistory.unshift(this.lastTraded);
        this.lastTraded = lastTradedPrice;
    }

    getPriceHistory() {
        return {
            history: this.priceHistory,
            lastTraded: this.lastTraded,
            ask: this.getAskPrice(),
            bid: this.getBidPrice(),
            delta: this.priceHistory[this.priceHistory.length -1] - this.lastTraded,
            volume: this.volume
        }
    }

    updateDividend() {
        this.dividendTime = Math.floor(Date.now() / 1000 + this.dividendInterval);
    }

    dividendDue() {
        return this.dividendTime < Date.now() / 1000;
    }

    getDividend() {
        return this.dividend;
    }
}