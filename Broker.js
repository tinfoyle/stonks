/**
 * Broker
 * Interface between Market and ClearingHouse
 */
class Broker {
    constructor(brokerId, brokerName, Market) {
        /**
         * Broker ID
         * 
         * @type {number}
         */
        this.id = brokerId;

        /**
         * Broker Name
         * 
         * @type {string}
         */
        this.name = brokerName;

        /**
         * List of Incoming Orders
         * @type {Array}
         */
        this.incomingOrders = [];

        /**
         * List of Outgoing Orders
         */
        this.outgoingOrders = [];

        /**
         * Market Instance
         * @type {Market}
         */
        this.Market = Market;
    }

    tick() {
        this.processIncomingOrders();
        this.Market.securities.forEach(security => {
            this.triggerOrderProcessing(security);
            security.tick();
        });
        this.processOutgoingOrders();
        this.processDividends();
    }

    placeOrder(agentId, security, quantity, price, orderType, orderExecutionType) {
        let orderId = this.generateOrderId();
        let order = new Order({
            orderId: orderId,
            agentId: agentId,
            ticker: security,
            quantity: quantity,
            price: price,
            type: orderType,
            executionType: orderExecutionType,
            status: "open"
        });
        this.incomingOrders.push(order);
    }

    generateOrderId() {
        return this.Market.getNextOrderId();
    }

    processIncomingOrders() {
        /**
         * Add Incoming Orders to ClearingHouse
         */
        this.incomingOrders.forEach(order => {
            let clearingHouse = this.Market.getClearingHouseByTicker(order.ticker);
            clearingHouse.addOrder(order);
        });
        this.incomingOrders = [];
    }

    processOutgoingOrders() {
        /**
         * Add Outgoing Orders to Market
         */
        let Market = this.Market;

        for (let index in this.outgoingOrders) {
            let order = this.outgoingOrders[index];
            let tradePrice = order.price;
            if (order.executionType === "market") {
                tradePrice = parseFloat(order.getTradePrice()).toFixed(2);
            }
            Market.getSecurityByTicker(order.ticker).updateLastTraded(tradePrice);
            let agent = Market.getAgentById(this.outgoingOrders[index].agentId);
            agent.portfolio.addCompletedOrder(this.outgoingOrders[index]);
            if (this.outgoingOrders[index].type === "sell") {
                agent.updateBalance(parseFloat(tradePrice * this.outgoingOrders[index].quantity).toFixed(2));
            }
        }
        this.outgoingOrders = [];
    }

    triggerOrderProcessing(security) {
        let clearingHouse = this.Market.getClearingHouseByTicker(security.ticker);
        clearingHouse.processOpenOrders();
        let newOutgoingOrders = clearingHouse.getClosedOrders();
        if (newOutgoingOrders.length > 0) {
            for (let order in newOutgoingOrders) {
                this.outgoingOrders.push(newOutgoingOrders[order]);
            }
        }
        clearingHouse.clearClosedOrders();
    }

    getPendingOrders(Market, agent) {
        let pendingOrders = [];
        for (let security of Market.securities) {
            let clearingHouse = security.clearingHouse;
            let orders = clearingHouse.getOrdersByAgentId(agent.id);
            if (orders.length > 0) {
                pendingOrders = pendingOrders.concat(orders);
            }
        }
        return pendingOrders;
    }

    cancelOrder(Market, orderId) {
        let security = Market.getSecurityByOrderId(orderId);
        let clearingHouse = Market.getClearingHouseByTicker(security.ticker);
        let order = clearingHouse.getOrderById(orderId);
        let agent = Market.getAgentById(order.agentId);
        let canceled = clearingHouse.removeOrder(orderId, order);
        if (canceled) {
            if (order.type === "buy") {
                agent.updateBalance((order.price * order.quantity).toFixed(2));
            }
        }
    }

    processDividends() {
        // Loop through securities to check if dividend due
        for (let security of this.Market.securities) {
            if (security.dividendDue()) {
                // Loop through agents to check if they own any shares
                for (let agent of this.Market.engine.Agents.agents) {
                    let shares = agent.portfolio.getSharesByTicker(security.ticker);
                    if (shares > 0) {
                        let dividend = parseFloat(security.getDividend()).toFixed(2);
                        agent.updateBalance(parseFloat(dividend * shares).toFixed(2));
                        this.Market.engine.Agents.agents[agent.id - 1] = agent;
                    }
                }
                security.updateDividend();
            }
        }
    }
}
