class Order {
    constructor(data) {
        /**
         * Order ID
         */
        this.id = data.orderId;

        /**
         * Agent ID
         */
        this.agentId = data.agentId;

        /**
         * Order Type
         * buy or sell
         */
        this.type = data.type;

        /**
         * Type of Execution
         * market or limit
         */
        this.executionType = data.executionType;

        /**
         * Order Status
         * open or closed or partial
         */
        this.status = data.status;

        /**
         * Order Price
         * Price of the order
         * If market order, this is the price of the security at the time of the order
         * If limit order, this is the price of the order
         */
        this.price = parseFloat(data.price).toFixed(2);

        /**
         * Order Quantity
         * Quantity of the order
         */
        this.quantity = data.quantity;

        /**
         * Order Time
         * Time of the order at submission
         */
        this.time = Date.now() / 1000;

        /**
         * Order Ticker
         * Ticker of the security
         */
        this.ticker = data.ticker;

        /**
         * Order Ledger
         * History of the order
         */
        this.ledger = [];

        /**
         * Order Validity
         */
        this.valid = this.validateOrder();

    }

    /**
     * Validate Order
     * Checks if the order is valid
     * @returns {boolean}
     */
    validateOrder() {
        if (this.type === "buy" || this.type === "sell") {
            if (this.executionType === "market" || this.executionType === "limit") {
                if (this.status === "open" || this.status === "closed") {
                    if (this.price > 0 && this.quantity > 0 && this.ticker !== "" && this.ticker !== undefined && this.ticker !== null) {
                        if (this.agentId && this.agentId !== undefined && this.agentId !== null) {
                            return true;
                        }
                    }
                }
            }
        }
        console.log("Order - Order Not Valid: ", this);
        return false;
    }

    /**
     * Returns the order ledger
     * @returns Array
     */
    getLedger() {
        return this.ledger;
    }

    /**
     * Adds to the order ledger
     */
    addLedgerEntry(entry) {
        this.ledger.push(entry);
    }

    /**
     * Returns the open quantity of the order
     */
    getOpenQuantity() {
        let openQuantity = this.quantity;
        for (let i = 0; i < this.ledger.length; i++) {
            openQuantity -= this.ledger[i].quantity;
        }
        if (openQuantity < 0) {
            openQuantity = 0;
        }
        return openQuantity;
    }

    /**
     * Returns the open value of the order
     */
    getOpenValue() {
        return this.price * this.getOpenQuantity();
    }

    /**
     * Closes against another order
     * @param {Integer} orderId
     * @param {Integer} quantity
     * @param {Float} price
     */
    close(orderId, quantity, price) {
        this.addLedgerEntry(this.createLedgerEntry(orderId, quantity, price));
        this.updateOrderStatus();
        return true;
    }

    /**
     * Creates a ledger entry
     * @param {Integer} orderId
     * @param {Integer} quantity
     * @param {Float} price
     * @returns {Object}
     */
    createLedgerEntry(orderId, quantity, price) {        
        return {
            quantity: quantity,
            price: price,
            time: Date.now() / 1000,
            orderId: orderId
        };
    }

    /**
     * Updates the order status
     */
    updateOrderStatus() {
        if (this.getOpenQuantity() === 0) {
            this.status = "closed";
        } else {
            this.status = "partial";
        }
    }

    getTradePrice() {
        if (this.ledger.length === 1) {
            return this.ledger[0].price;
        } else {
            let totalQuantity = 0;
            let totalPrice = 0;
            for (let i = 0; i < this.ledger.length; i++) {
                totalQuantity += this.ledger[i].quantity;
                totalPrice += this.ledger[i].quantity * this.ledger[i].price;
            }
            return totalPrice / totalQuantity;
        }

    }
}