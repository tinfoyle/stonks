/**
 * Main Market Class
 * 
 * Handles all game logic
 * 
 * @class Market
 */
class Market {
    constructor(engine) {
        this.config = engine.config;
        this.engine = engine;
        /**
         * Initialize
         */
        this.initialize();
    }

    initialize() {
        this.securities = [];
        this.modifiers = [];
        this.history = [];
        this.labels = [];
        this.datasets = [];
        this.nextOrderId = 0;
        this.broker = new Broker(0, 'First Broker', this);
    }

    generateFirstCompany() {
        let security = new Security("First Company", "FST", 105);
        this.securities.push(security);
    }

    getSecurityByTicker(ticker) {
        let security = this.securities.find(security => security.ticker === ticker);
        return security;
    }

    getAgentById(id) {
        let agent = this.engine.Agents.agents.find(agent => agent.id === id);
        return agent;
    }

    getSecurityByOrderId(orderId) {
        for (let security of this.securities) {
            let clearingHouse = this.getClearingHouseByTicker(security.ticker);
            let order = clearingHouse.getOrderById(orderId);
            if (order !== null) {
                return security;
            }
        }
        return null;
    }

    generateDefaultDataset() {
        this.datasets.push(
            {
                label: "Bid Price",
                data: [],
                backgroundColor: this.config.buyColor,
                borderColor: this.config.buyBorderColor,
                borderWidth: 1
            },
            {
                label: "Ask Price",
                data: [],
                backgroundColor: this.config.sellColor,
                borderColor: this.config.sellBorderColor,
                borderWidth: 1
            },
            {
                label: "Last Traded Price",
                data: [],
                backgroundColor: this.config.lastTradedColor,
                borderColor: this.config.lastTradedBorderColor,
                borderWidth: 1
            }
        );
    }

    tick(Engine) {
        this.Agents = Engine.Agents;
        this.broker.tick();
        let time = new Date().toLocaleTimeString();
        this.labels.push(time);
        this.datasets[0].data.push(this.securities[0].getBidPrice());
        this.datasets[1].data.push(this.securities[0].getAskPrice());
        this.datasets[2].data.push(this.securities[0].getLastTraded());
        Engine.UI.graphTick(this);
        this.cullExcessGraphPoints();
    }

    cullExcessGraphPoints() {
        if (this.datasets[0].data.length > this.config.maxGraphTicks) {
            this.datasets[0].data.shift();
            this.datasets[1].data.shift();
            this.datasets[2].data.shift();
            this.labels.shift();
        }
    }

    getClearingHouseByTicker(ticker) {
        let security = this.securities.find(security => security.ticker === ticker);
        if (security) {
            return security.clearingHouse;
        }
    }

    getNextOrderId() {
        return this.nextOrderId++;
    }
}