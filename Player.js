class Player {
    constructor(engine) {
        /**
         * Initialize
         */
        this.engine = engine;
        this.initialize();
    }

    initialize() {
        this.name = "Player";
        this.cash = 10000;
        this.portfolio = [];
        this.orders = [];
        this.history = [];
        this.upgrades = [];
    }
}