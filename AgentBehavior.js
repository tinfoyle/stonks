class AgentBehavior {
    constructor(agent, Market) {
        this.agent = agent;
        this.initialize(Market);
    }

    initialize(Market) {
        switch (this.agent.type) {
            case "Bank":
                this.bankInit();
                this.addStartingHoldings(Market);
                break;
            case "Investor":
                this.investorInit();
                break;
            case "Trader":
                this.traderInit();
                break;
            case "Retail":
                this.retailInit();
                break;
        }
    }

    bankInit() {
        this.holdingRatio = (Math.floor(Math.random() * 100) + 50) / 100;
        this.stingyNess = Math.floor((Math.random() * 50) - 10);
        this.agent.updateBalance(Math.floor(Math.random() * 50000).toFixed(2));
        this.patience = Math.floor(Math.random() * 100) + 25;
        this.impatience = 0;
    }

    investorInit() {
    }

    traderInit() {
    }

    retailInit() {
    }

    tick(Market) {
        switch (this.agent.type) {
            case "Bank":
                this.bankBehavior(Market);
                break;
            case "Investor":
                this.investorBehavior(Market);
                break;
            case "Trader":
                this.traderBehavior(Market);
                break;
            case "Retail":
                this.retailBehavior(Market);
                break;
        }
    }

    bankBehavior(Market) {
        /**
         * Bank Behavior
         * Banks will buy securities if they have low price action
         * Banks hold securities for a long period of time
         * Banks will try to maintain a portfolio to balance ratio
         * Will Sell some securities to maintain balance ratio
         * Will Buy some securities to maintain balance ratio
         */
        let idealHoldingsValue = this.holdingRatio * this.agent.balance;
        let currentHoldingsValue = this.agent.portfolio.getValueOfHoldings();
        if (idealHoldingsValue > currentHoldingsValue) {
            let security = this.getRandomSecurity(Market);
            let idealOrderValue = 0.1 * this.agent.balance;
            let targetPrice = this.getTargetPrice(security, "buy");
            let idealOrderQuantity = Math.floor(idealOrderValue / targetPrice);
            if (idealOrderQuantity > 0) {
                let executionType = "limit";
                if (this.impatience > 0) {
                    executionType = "market";
                    this.impatience = 0;
                }
                this.agent.placeOrder(Market, security.ticker, idealOrderQuantity, targetPrice, "buy", executionType);
            }
        } else if (idealHoldingsValue < currentHoldingsValue) {
            let idealOrderValue = 0.1 * this.agent.balance;
            let holding = this.getRandomHolding().ticker;
            let targetPrice = this.getTargetPrice(Market.getSecurityByTicker(holding), "sell");
            let idealOrderQuantity = Math.floor(idealOrderValue / targetPrice);
            if (idealOrderQuantity > 0) {
                let executionType = "limit";
                if (this.impatience > 0) {
                    executionType = "market";
                    this.impatience = 0;
                }
                this.agent.placeOrder(Market, holding, idealOrderQuantity, targetPrice, "sell", executionType);
            }
        }

        /**
         * Check Existing Pending Orders
         */
        let pendingOrders = Market.broker.getPendingOrders(Market, this.agent);
        for (let order of pendingOrders) {
            if (((Date.now() / 1000) - order.time) > this.patience) {
                this.agent.cancelOrder(Market, order);
                this.impatience++;
            }
        }
    }

    investorBehavior(Market, agent) {
        /**
         * Investor Behavior
         * Investors will buy securities when they have decresed in price
         * Investors will sell securities on a higher profit margin
         */
        return;
    }

    traderBehavior(Market, agent) {
        /**
         * Trader Behavior
         * Traders will buy securities when they are volitile and have decreased in price
         * Traders will sell securities when they have increased in a small profit margin
         */
        return;
    }

    retailBehavior(Market, agent) {
        /**
         * Retail Behavior
         * 
         * Retail will buy securities when they are trending upwards
         * Retail will sell securities when they are trending downwards
         */
        return;
    }

    hasHolding() {
        return this.agent.portfolio.holdings.length > 0;
    }

    getRandomSecurity(Market) {
        return Market.securities[Math.floor(Math.random() * Market.securities.length)];
    }

    getRandomHolding() {
        let holdings = this.agent.portfolio.holdings;
        /**
         * TODO: Actually pull a random holding
         */
        return holdings["FST"];
    }

    getHoldingsRatio(Market) {
        let holdingsQuantity = this.agent.portfolio.getHoldingsQuantity();
        let currentPrices = {}
        for (let holding in holdingsQuantity) {
            currentPrices[holding] = Market.getSecurityByTicker(holding).getLastTraded();
        }
        let totalValue = 0;
        for (let holding in holdingsQuantity) {
            totalValue += holdingsQuantity[holding] * currentPrices[holding];
        }
        if (totalValue === 0) {
            return 0;
        }

        return this.agent.balance / totalValue;
    }

    addStartingHoldings(Market) {
        let holdingQuantity = Math.floor(Math.random() * 1000);
        let holdingValue = Math.floor(Math.random() * 100);
        this.agent.portfolio.addCompletedOrder({
            ticker: this.getRandomSecurity(Market).ticker,
            quantity: holdingQuantity,
            type: "buy",
            price: holdingValue,
            time: Date.now() / 1000
        });
    }

    getTargetPrice(security, type) {
        let modifier = parseFloat(this.stingyNess / 100).toFixed(2);
        let targetPrice = 0;
        if (type === "buy") {
            targetPrice = parseFloat(parseFloat(security.getAskPrice()) + modifier).toFixed(2);
        }
        if (type === "sell") {
            targetPrice = parseFloat(parseFloat(security.getBidPrice()) - modifier).toFixed(2);
        }
        return targetPrice;
    }
}