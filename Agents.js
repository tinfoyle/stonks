/**
 * Agent Factory
 */
class Agents {
    constructor() {
        this.currentAgentId = 0;
        this.agents = [];
    }

    getAgents() {
        return this.agents;
    }

    getNewAgentId() {
        this.currentAgentId++;
        return this.currentAgentId;
    }
    generateNewAgents(amount, Market) {
        for (let i = 0; i < amount; i++) {
            let agentId = this.getNewAgentId();
            let agentType = this.getRandomAgentType();
            this.agents.push(new Agent({
                agentId: agentId,
                agentName: agentType + " Agent " + agentId,
                agentType: agentType,
                agentBalance: 0.00,
                agentOrders: [],
                agentHoldings: [],
                portfolio: new Portfolio(),
                Market: Market
            }));
            /**
             * Disabled for now
             */
            //this.generateExistingHoldings(Market, agentId);
        }
    }

    generateExistingHoldings(Market, agentId) {
        let random = Math.floor(Math.random() * 2);
        if (random === 1) {
            let randomQuantity = Math.floor(Math.random() * 100);
            let randomPrice = Math.floor(Math.random() * 100);
            let order = new Order({
                orderId: Market.getNextOrderId(),
                agentId: agentId,
                type: "buy",
                status: "Closed",
                price: randomPrice,
                quantity: randomQuantity,
                ticker: Market.securities[0].ticker,
                time: 0
            });
            this.agents.find(agent => agent.agentId === agentId).agentHoldings.push(order);
        }
    }

    getRandomAgentType() {
        let types = ["Bank", "Investor", "Trader", "Retail"];
        let randomIndex = Math.floor(Math.random() * types.length);

        if (randomIndex === 2) {
            return "Bank";
        }
        return types[randomIndex];
    }

    tick(Market) {
        for (let agent of this.agents) {
            agent.tick(Market);
        }
    }
}