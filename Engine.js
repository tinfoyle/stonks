class Engine {
    constructor() {
        /**
         * Initialize
         * Instantiates Sub-Classes
         */
        this.config = new Config();
        this.Market = new Market(this);
        this.Agents = new Agents(this.config);
        this.initialize();
    }

    /**
     * Set Default Values
     */
    initialize() {
        // Set Default Values
        this.config.setMetaData();
        this.loadEmptyGame();
    }

    loadEmptyGame() {
        this.Market.generateDefaultDataset();
        this.Player = new Player(this);
        this.UI = new Ui(this);
        this.UI.drawGraph(this.Market.labels, this.Market.datasets);
        this.Market.generateFirstCompany();
        this.UI.statusMessage('Created First Company');
        this.Agents.generateNewAgents(this.config.numAgents, this.Market);
        this.UI.statusMessage('Created 100 Agents');
        this.UI.statusMessage("New Game Loaded");
    }

    tick() {
        this.Market.tick(this);
        this.Agents.tick(this.Market);
    }
}