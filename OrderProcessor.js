class OrderProcessor {
    constructor() {
        this.orderA = null;
        this.orderB = null;
        this.tradePrice = 0;
    }

    addOrders(orderA, orderB) {
        this.orderA = orderA;
        this.orderB = orderB;
    }

    processOrders() {
        if (this.validateOrders()) {
            return this.executeOrders();
        } else {
            return false;
        }
    }

    validateOrders() {
        if (this.validateSecurities()) {
            if (this.validateOrderTypes()) {
                if (this.validateOrderPrices()) {
                    return true;
                } else {
                    console.log("Order Process - Order Prices Not Valid", this.orderA, this.orderB);
                }
            } else {
                console.log("Order Process - Order Types Not Valid", this.orderA, this.orderB);
            }
        } else {
            console.log("Order Process - Order Securities Not Valid", this.orderA, this.orderB);
        }
        return false;
    }

    validateSecurities() {
        if (this.orderA.security === this.orderB.security) {
            return true;
        }
        return false;
    }

    adjustBidAsk() {
        if (this.orderA.type === "sell") {
            this.orderB.price = this.orderA.price;
        } else {
            this.orderA.price = this.orderB.price;
        }
    }

    validateOrderTypes() {
        if (this.orderA.type === "buy" && this.orderB.type === "sell") {
            return true;
        } else if (this.orderA.type === "sell" && this.orderB.type === "buy") {
            return true;
        }
        return false;
    }

    validateOrderPrices() {
        if (this.orderA.executionType === "market" || this.orderB.executionType === "market") {
            if (this.orderA.executionType === "market") {
                this.orderA.price = this.orderB.price;
            }
            if (this.orderB.executionType === "market") {
                this.orderB.price = this.orderA.price;
            }
            return true;
        } else if (this.orderA.executionType === "limit" && this.orderB.executionType === "limit") {
            if (this.orderA.price === this.orderB.price) {
                return true;
            } else {
                this.adjustBidAsk();
                if (this.orderA.price === this.orderB.price) {
                    return true;
                }
            }
        }
        return false;
    }

    executeOrders() {
        let orderAOpenQuantity = this.orderA.getOpenQuantity();
        let orderBOpenQuantity = this.orderB.getOpenQuantity();
        let orderAClosed = this.orderA.close(this.orderB.id, orderBOpenQuantity, this.orderB.price);
        let orderBClosed = this.orderB.close(this.orderA.id, orderAOpenQuantity, this.orderA.price);
        if (orderAClosed && orderBClosed) {
            return true;
        } else {
            console.log("Orders Not Closed", this.orderA, this.orderB);
            return false;
        }
    }

    hasOrders() {
        if (this.orderA && this.orderB) {
            return true;
        }
        return false;
    }

    clearOrders() {
        this.orderA = null;
        this.orderB = null;
        this.tradePrice = 0;
    }
}