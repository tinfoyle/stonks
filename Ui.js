class Ui {
    constructor() {
        if (typeof($) === "undefined") {
            throw new Error("jQuery is not loaded");
        }
        this.initialize();
    }

    initialize() {
        this.setDataTableConfig();
        this.loadWindow();
        this.statusMessage("UI Initialized");
    }

    setDataTableConfig() {
        this.dataTableConfig = {
            'columns': [
                { 'width': '10%' },
                { 'width': '90%' },
            ],
            'scrollY': '200px',
            'scrollCollapse': true,
            'bDestroy': true,
        };
        this.drawnLabels = [];
        this.drawnData = [];
    }
    
    loadWindow() {
        this.window = this.createGameWindow();
        this.context = this.createContet();
        this.portfolio = this.createPortfolio();
        this.statusLog = this.createStatusLog();
        this.market = this.createMarket();
        this.graph = this.createGraphCanvas()
        
        this.window.appendChild(this.graph);
        this.window.appendChild(this.context);
        this.window.appendChild(this.portfolio);
        this.window.appendChild(this.market);
        this.window.appendChild(this.statusLog);

        this.appendWindow();
        this.loadStatusLog();
    }

    drawGraph(labels, datasets) {
        const ctx = document.getElementById('game-graph').getContext('2d');
        this.graphChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: labels,
                datasets: datasets
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: false
                    }
                },
                animations: false
            },
        });

    }
    appendWindow() {
        document.body.style.height = "100%";
        document.body.appendChild(this.window);
    }
    createGameWindow() {
        let gameWindow = document.createElement("div");
        gameWindow.id = "game-window";
        gameWindow.style.height = "100%";
        return gameWindow;
    }

    createStatusLog() {
        let statusLog = document.createElement("table");
        statusLog.id = "game-status-log";
        let thead = document.createElement("thead");
        let tr = thead.insertRow();
        let timeHeader = document.createElement("th");
        timeHeader.innerHTML = "Time";
        let messageHeader = document.createElement("th");
        messageHeader.innerHTML = "Message";
        tr.appendChild(timeHeader);
        tr.appendChild(messageHeader);
        statusLog.append(thead);
        let tbody = document.createElement("tbody");
        statusLog.append(tbody);
        statusLog.classList.add('display');
        return statusLog;
    }
    createGraphCanvas() {
        this.chartCanvas = document.createElement("canvas");
        this.chartCanvas.id = "game-graph";
        this.chartCanvas.height = "500";
        this.chartCanvas.width = Math.floor(window.innerWidth * .95);
        this.chartCanvas.style = "margin-left: auto; margin-right: auto; display: block;";
        return this.chartCanvas;
    }

    graphTick(Market) {
        this.graphChart.data.labels = Market.labels;
        this.graphChart.data.datasets = Market.datasets;
        this.graphChart.update();
    }

    createMarket() {
        let market = document.createElement("div");
        market.id = "game-market";
        return market;
    }

    createPortfolio() {
        let portfolio = document.createElement("div");
        portfolio.id = "game-portfolio";
        return portfolio;
    }

    createContet() {
        let context = document.createElement("div");
        context.id = "game-context";
        return context;
    }

    loadStatusLog() {
        $("#game-status-log").DataTable(this.dataTableConfig).draw();
    }

    statusMessage(message) {
        let table = $("#game-status-log").DataTable(this.dataTableConfig);
        let time = new Date().toLocaleTimeString();
        table.row.add([time, message]).draw();
    }
}